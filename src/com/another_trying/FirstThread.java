package com.another_trying;

/**
 * Created by hudrogen on 06.04.17.
 */
public class FirstThread implements Runnable{
    Q q;

    public FirstThread(Q q) throws InterruptedException {
        this.q = q;
        new Thread(this).start();
    }

    @Override
    public void run() {
        synchronized (q) {
            while (true) {
                q.printNumbers();
            }
        }

    }
}


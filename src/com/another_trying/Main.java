package com.another_trying;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hudrogen on 06.04.17.
 */
public class Main {
    public static void main(String[] args) {
        Q q = new Q();
//        ExecutorService executor = Executors.newFixedThreadPool(3);
//        try {
//            executor.execute(new FirstThread(q));
//            executor.execute(new SecondThread(q));
//            executor.execute(new ThirdThread(q));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        try {
            new FirstThread(q);
            new SecondThread(q);
            new ThirdThread(q);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

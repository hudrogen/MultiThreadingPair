package com.another_trying;

/**
 * Created by hudrogen on 06.04.17.
 */
public class SecondThread implements Runnable{
    Q q;
    long beginTime;

    public SecondThread(Q q) throws InterruptedException {
        this.q = q;
        new Thread(this, "Каждые 5 сек").start();
    }

    @Override
    public void run() {
        synchronized (q){
        while (true) {
                q.printAlert5();
        }
    }
    }
}
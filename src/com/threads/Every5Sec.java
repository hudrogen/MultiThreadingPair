package com.threads;


public class Every5Sec implements Runnable{

    private  EverySec es;

    public Every5Sec(EverySec es){
        this.es = es;
    }

    @Override
    public void run() {

        int oldTime = 0;
        int newTime = 0;

        while (true)
        {
            newTime = es.getTime();
            if (newTime % 5 == 0 && newTime != 0 && newTime != oldTime)
            {
                System.out.println("Прошло 5 сек");
                oldTime = newTime - 1;
            }
        }
    }
}

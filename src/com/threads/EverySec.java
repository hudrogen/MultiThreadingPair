package com.threads;


import java.util.Date;

import static java.lang.Thread.sleep;

public class EverySec implements Runnable{

    long diff;
    public synchronized int getTime(){
        return (int)diff;

    }

    @Override
    public void run() {
        long startSession = System.currentTimeMillis();
        while (true){
            diff = (System.currentTimeMillis() - startSession)/1000;
            System.out.println((int)diff);
            mySleep(1000);

        }

    }

    private void mySleep(int millisec){
        try {
            sleep(millisec);
        } catch (InterruptedException e) {
            System.out.println("Что-то пошло не так со sleep");
        }
    }
}

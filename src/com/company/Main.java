package com.company;

import com.threads.Every5Sec;
import com.threads.EverySec;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ExecutorService executor = Executors.newFixedThreadPool(2);
        EverySec es = new EverySec();
        Every5Sec e5s = new Every5Sec(es);

        executor.execute(es);
        executor.execute(e5s);
        //Thread est = new Thread(es);
        //est.start();

        //System.out.println("Главный поток завершен");
    }
}
